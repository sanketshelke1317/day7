﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactPro.Exceptions
{
    internal class ContactNotFound:Exception
    {
        public ContactNotFound()
        {

        }
        public ContactNotFound(string message):base(message)
        {
            
        }
    }
}
