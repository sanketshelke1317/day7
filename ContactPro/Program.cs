﻿
using ContactPro.Exceptions;
using ContactPro.Model;
using ContactPro.Repository;




ContactRepository contactRepository = new ContactRepository();
START: 
Console.WriteLine($"0) GetAllContacts\n1) Add Contact\n2) Update Contact\n3) Delete Contact\n4) Filter City");
try
{
    switch (Convert.ToInt32(Console.ReadLine()))
    {
        case 0:
            List<Contact> contactArray = contactRepository.GetAllContacts();

            foreach (Contact contact1 in contactArray)
            {
                Console.WriteLine(contact1);
            }
            goto START;
        case 1:
            contactRepository.AddContact(contactRepository.EnterContactDetails());
            goto START;
        case 2:

            contactRepository.UpdateContact(contactRepository.EnterContactDetails());
            goto START;
        case 3:
            Console.WriteLine("Enter Name of Contact to Delete");
            contactRepository.DeleteContact(Console.ReadLine());
            goto START;
        case 4:
            Console.WriteLine("Enter City Name");
            contactRepository.GetContactByCity(Console.ReadLine());
            //List<Contact> contact2 = contactRepository.GetContactByCity(Console.ReadLine());
            //foreach(Contact contact in contact2)
            //{
            //    Console.WriteLine(contact);
            //}
            goto START;
        default:
            break;
    }
}
catch (ContactNotFound e)
{
    Console.WriteLine(e.Message);
    goto START;
}
