﻿using ContactPro.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactPro.Exceptions;
namespace ContactPro.Repository
{
    internal class ContactRepository
    {
        List<Contact> contacts;

        public ContactRepository()
        {
            contacts = new List<Contact>
            {
                new Contact(){Name="Jadu", Address="Mars", City="Don't Know",PhoneNumber=00000000000},
                new Contact(){Name="Joker", Address="Gotham", City="Gotham", PhoneNumber=99999999999}
            };
        }
        public void AddContact(Contact contact)
        {
            contacts.Add(contact);
            Console.WriteLine("Contact Added Successfully");
        }
        public void DeleteContact(string name)
        {
            contacts.Remove(GetContactByName(name));
            Console.WriteLine("Contact Deleted Successfully");
        }

        private Contact GetContactByName(string name)
        {
            if (null == contacts.Find(x => x.Name == name))
            {
                Console.WriteLine("Test");
                throw (new ContactNotFound("Contact Not Found"));
            }
            else
            {
                return contacts.FirstOrDefault(x => x.Name == name);
            }
        }

        internal void UpdateContact(Contact contact)
        {
            Contact myContact = GetContactByName(contact.Name);
            myContact.Address = contact.Address;
            myContact.PhoneNumber = contact.PhoneNumber;
            myContact.City = contact.City;
        }

        public List<Contact> GetAllContacts()
        {
            return contacts;
        }

        public Contact EnterContactDetails()
        {
            Contact contact = new Contact();
            Console.WriteLine("Enter Name");
            contact.Name = Console.ReadLine();
            Console.WriteLine("Enter City Name");
            contact.City = Console.ReadLine();
            Console.WriteLine("Enter Address");
            contact.Address = Console.ReadLine();
            Console.WriteLine("Enter Phone Number");
            contact.PhoneNumber = Convert.ToInt64(Console.ReadLine());
            return contact;
        }

        public void GetContactByCity(string city)
        {
            var i = (from p in contacts
                     group p by p.City == city into g
                     select g).ToList();
            foreach (var contact in i)
            {

                foreach (var contact2 in contact)
                {
                    if (contact.Key) Console.WriteLine(contact2);
                    
                }
            }

        }
    }
}
