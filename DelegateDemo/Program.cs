﻿class Deligatee
{
    public static void Main()
    {
        Action<int, int> sum = (a, b) => Console.WriteLine(a+b);
        sum(2, 3);
        Func<int, int, int> multi = (a, b) => a * b;
        Console.WriteLine(multi(2,3));

        Predicate<int> predicate = (a) => a>0 ;
        Console.WriteLine("Enter Number");
        if (predicate(Convert.ToInt32(Console.ReadLine())))
        {
            Console.WriteLine("Greater than Zero");
        }
        else
        {
            Console.WriteLine("Less than zero");
        }
    }
}