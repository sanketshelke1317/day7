﻿using JoinOperation.Model;

List<Product> products = new List<Product>
{
    new Product{Id = 1, Name = "Apple",Price=20000},
    new Product{Id = 2, Name = "Mac",Price=10000},
    new Product{Id = 3, Name = "Iphone",Price=290000}
};
List<ProductQuantity> productQuantity = new List<ProductQuantity>
{
    new ProductQuantity{Name = "Apple",Quantity=20},
    new ProductQuantity{Name = "Mac",Quantity=10},
    new ProductQuantity{Name = "Iphone",Quantity=29}
};


var joinProducts = from p in products
        join b in productQuantity
        on p.Name equals b.Name
        select new { productName = p.Name, productQuantity = b.Quantity };


foreach(var j in joinProducts)
{
    Console.WriteLine($"{j.productName} \t {j.productQuantity}");
}


