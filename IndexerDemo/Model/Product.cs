﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexerDemo.Model
{
    internal class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }



        public object this[int index]
        {
            get {
                if (index == 0) return Id;
                else if(index == 1) return Name;
                else return Price;
            }
            set {
                if (index == 0) Id = (int)value;
                else if(index == 1) Name = (string)value;
                else Price = (int)value;
            }
        }
        
    }
    
}
