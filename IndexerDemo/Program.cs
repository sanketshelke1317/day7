﻿using IndexerDemo.Model;

Product product = new Product() { Id = 1, Name = "Iphone", Price = 50000 };
Console.WriteLine($"id = {product[0]} \t Name = {product[1]} \t Price = {product[2]}");

product[0] = 2;
product[1] = "MacBook";
product[2] = 20000;

Console.WriteLine($"id = {product[0]} \t Name = {product[1]} \t Price = {product[2]}");
